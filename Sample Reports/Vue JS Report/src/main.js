import { createApp } from 'vue'
import App from '@/App'
import 'tailwindcss/tailwind.css'

const app = createApp(App)
app.mount('#app')
